import os
import sys
import itertools
import pickle


class Window():
    def __init__(self, name, workspace_number, x, y, w, h):
            self.name = name
            self.workspace_number = workspace_number
            self.x = x
            self.y = y
            self.w = w
            self.h = h
    
    def __repr__(self):
        return self.__str__()
    
    def __str__(self):
        return "Window Title: {}\nWorkspace Number: {}\nPosition: {},{}\nGeometry: {}x{}\n".format(self.name, self.workspace_number, self.x, self.y, self.w, self.h)

workspace_count = os.popen('xdotool get_num_desktops').read()

windows = dict()


def windows_save():
    for i in range(int(workspace_count)):
        window_names = os.popen('xdotool search --desktop {} --onlyvisible --name "[a-z]" getwindowname %@'.format(i)).read().split('\n')
        window_geometries = os.popen('xdotool search --desktop {} --onlyvisible --name "[a-z]" getwindowgeometry %@'.format(i)).read().split('\nW')
        for name, geometry in zip(window_names, window_geometries):
            if not name:
                continue
            if 'New Tab' in name:
                name = 'New Tab Workspace {} - Google Chome'.format(i)
                os.popen('xdotool search --desktop {} --onlyvisible --name "New Tab" set_window --name "{}"'.format(i, name))
            _, position, _, geometry = geometry.split(':')
            x, y = position.split('(')[0].split(',')
            w, h = geometry.split('x')
            windows[name] = Window(name, i, x, y, w, h)

    with open('/home/deeb/scripts/windows.txt', 'wb') as f:
        pickle.dump(windows, f, pickle.HIGHEST_PROTOCOL)


def windows_load():
    with open('/home/deeb/scripts/windows.txt', 'rb') as f:
        windows = pickle.load(f)
        
    error_message = '(out of range)'
    i = 0
    while True:
        i += 1
        name = os.popen('xdotool search --onlyvisible --name "[a-z]" getwindowname %{}'.format(i)).read().strip()
        if not name: # out of range
            break
        try:
            window = windows[name]
            os.popen('xdotool search --onlyvisible --name "^{}$" set_desktop_for_window {} windowmove {} {} windowsize {} {}'.format(name, window.workspace_number, window.x, window.y, window.w, window.h))
        except KeyError:
            print('{} not found in saved windows.'.format(name))

if ''.join(sys.argv[1:]) == 's':
    windows_save()
elif ''.join(sys.argv[1:]) == 'l':
    windows_load()

