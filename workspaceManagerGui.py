import gi
import subprocess
import os

gi.require_version('Gtk','3.0')
from gi.repository import Gtk as gtk


class WorkspaceManagerGui(gtk.Window):
   def __init__(self, is_single_monitor):
      super(WorkspaceManagerGui, self).__init__()
      self.set_default_size(330, 200)
      self.workspace = 0 # 0 save, load, neither
      self.monitor = 0 # 0 off, on, neither
      self.power = 2 # 2 shutdown, restart, neither
      self.dota = not is_single_monitor
      self.chrome = not is_single_monitor
      self.set_title('Workspace Manager GUI')      
      rdoSaveWorkspace = gtk.RadioButton('Save Workspace')
      rdoSaveWorkspace.connect('toggled', self.set_workspace, 1)
      rdoLoadWorkspace = gtk.RadioButton.new_from_widget(rdoSaveWorkspace)
      rdoLoadWorkspace.set_label('Load Workspace')
      rdoLoadWorkspace.connect('toggled', self.set_workspace, 1)
      rdoWorkspaceDoNeither = gtk.RadioButton.new_from_widget(rdoSaveWorkspace)
      rdoWorkspaceDoNeither.set_label('Do Neither')
      rdoWorkspaceDoNeither.connect('toggled', self.set_workspace, 1)
      rdoSaveWorkspace.set_active(True)
      rdoLoadWorkspace.set_active(is_single_monitor)      
      rdoMonitorOff = gtk.RadioButton(label='HDMI Off')
      rdoMonitorOff.connect('toggled', self.set_monitor, 1)
      rdoMonitorOn = gtk.RadioButton.new_from_widget(rdoMonitorOff)
      rdoMonitorOn.set_label('HDMI On')
      rdoMonitorOn.connect('toggled', self.set_monitor, 1)
      rdoMonitorDoNeither = gtk.RadioButton.new_from_widget(rdoMonitorOff)
      rdoMonitorDoNeither.set_label('Do Neither')
      rdoMonitorDoNeither.connect('toggled', self.set_monitor, 1)
      rdoMonitorOff.set_active(True)
      rdoMonitorOn.set_active(is_single_monitor)      
      rdoShutdown = gtk.RadioButton(label='Shutdown')
      rdoShutdown.connect('toggled', self.set_power, 1)
      rdoRestart = gtk.RadioButton.new_from_widget(rdoShutdown)
      rdoRestart.set_label('Restart')
      rdoRestart.connect('toggled', self.set_power, 1)
      rdoPowerDoNeither = gtk.RadioButton.new_from_widget(rdoShutdown)
      rdoPowerDoNeither.set_label('Do Neither')
      rdoPowerDoNeither.connect('toggled', self.set_power, 1)
      rdoPowerDoNeither.set_active(True)      
      chkDota = gtk.CheckButton(label='Launch DotA')
      chkDota.set_active(not is_single_monitor)
      chkDota.connect('toggled', lambda x:self.set_dota(chkDota.get_active()))
      chkChrome = gtk.CheckButton(label='Close Chrome')
      chkChrome.set_active(not is_single_monitor)
      chkChrome.connect('toggled', lambda x:self.set_chrome(chkChrome.get_active()))
      btnQuit = gtk.Button('Quit')
      btnQuit.connect("clicked", gtk.main_quit)
      btnSubmit = gtk.Button('Submit')
      btnSubmit.connect("clicked", lambda _: self.submit())
      grid = gtk.Grid.new()
      grid.attach(rdoSaveWorkspace, 1, 0, 1, 1)
      grid.attach(rdoLoadWorkspace, 1, 1, 1, 1)
      grid.attach(rdoWorkspaceDoNeither, 1, 2, 1, 1)
      grid.attach(rdoMonitorOff, 0, 0, 1, 1)
      grid.attach(rdoMonitorOn, 0, 1, 1, 1)
      grid.attach(rdoMonitorDoNeither, 0, 2, 1, 1)
      grid.attach(chkDota, 1, 4, 1, 1)
      grid.attach(chkChrome, 0, 4, 1, 1)
      grid.attach(btnQuit, 0, 5, 1, 1)
      grid.attach(btnSubmit, 1, 5, 1, 1)
      grid.attach(rdoShutdown, 0, 6, 1, 1)
      grid.attach(rdoRestart, 0, 7, 1, 1)
      grid.attach(rdoPowerDoNeither, 0, 8, 1, 1)
      self.add(grid)
      self.show_all()
      
   def set_workspace(self, button, val):
      if button.get_label() == 'Load Workspace':
         self.workspace = 1
      elif button.get_label() == 'Save Workspace':
         self.workspace = 0
      else:
         self.workspace = 2
         
   def set_monitor(self, button, val):
      if button.get_label() == 'HDMI Off':
         self.monitor = 0
      elif button.get_label() == 'HDMI On':
         self.monitor = 1
      else:
         self.monitor = 2
         
   def set_power(self, button, val):
      if button.get_label() == 'Shutdown':
         self.power = 0
      elif button.get_label() == 'Restart':
         self.power = 1
      else:
         self.power = 2
   
   def set_dota(self, val):
      self.dota = val
   
   def set_chrome(self, val):
      self.chrome = val
      
   def submit(self):
      if self.workspace == 0:
         subprocess.call('python3 /home/deeb/scripts/workspaceManager.py s', shell=True)
      if self.monitor == 1:
         subprocess.call('xrandr --output HDMI-A-0 --mode 1920x1080 --left-of eDP && sleep 3 && xrandr --output HDMI-A-0 --brightness 0.4', shell=True)
      elif self.monitor == 0:
         subprocess.call('xrandr --output HDMI-A-0 --off', shell=True)
      if self.workspace == 1:
         subprocess.call('python3 /home/deeb/scripts/workspaceManager.py l', shell=True)
      if self.chrome:
         subprocess.call('killall -s 9 chrome', shell=True)
      if self.dota:
         subprocess.Popen('steam steam://rungameid/570', shell=True)
         # subprocess.Popen('/home/deeb/.steam/steam/ubuntu12_32/steam-runtime/run.sh "/home/deeb/.steam/steam/steamapps/common/dota 2 beta/game/dota.sh"', shell=True)
      if self.power == 0:
         subprocess.call('shutdown now', shell=True)
      elif self.power == 1:
         subprocess.call('shutdown -r now', shell=True)

is_single_monitor = os.popen('xrandr --listactivemonitors').read().split('\n')[0][-1]
is_single_monitor = bool(int(is_single_monitor) % 2)
    
app = WorkspaceManagerGui(is_single_monitor)
app.connect('destroy', gtk.main_quit)
gtk.main()

# todo have multiple windows.txt in workspaceManager.py and as different args choose different layotus

